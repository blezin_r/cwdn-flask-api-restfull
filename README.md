Vous avez pu voir une architecture MVC, mais aujourd'hui beaucoup de projets proposent ou utilisent une architecture centrée sur des APIs, exemple, des applications mobiles qui récupèrent des données via ce genre de solution.

Vous devrez donc fournir un ensemble de Endpoint pour que d'autres personnes puissent utiliser votre CWDN.

Le choix de la techno est complètement libre, vous pouvez donc partir sur du NodeJS ou du Java, en passant par du Ruby ou rester sur du PHP, de la même manière les frameworks sont complètement autorisés.

Attention, cependant dans ce projet aucune aide ne vous sera donnée sur la techno, donc si vous utilisez une techno que vous ne maîtrisez pas pour faire un "test", ce sera à vos risques et périls.

Le projet se concentrera sur l'API, et pour perdre le moins de temps possible, un fichier SQL avec une structure et des données de test vous seront fourni.

Si vous avez envie de modifier la structure, c'est à vous de voir, mais pareil c'est à vos risques et périls.