from flask import Flask
from flask_mysqldb import MySQL

def my_app():
    """
    Initialize the app variable
    """
    app = Flask(__name__)
    app.config['MYSQL_HOST'] = "localhost"
    app.config["MYSQL_PORT"] = 3306
    app.config["MYSQL_USER"] = "root"
    app.config["MYSQL_PASSWORD"] = "root"
    app.config["MYSQL_DB"] = "etna_crowding"
    mysql = MySQL(app)

    return [app, mysql]