from flask import jsonify, request, Response, redirect
from flask_mysqldb import MySQL
from collections import OrderedDict
import requests
import re
import sys
import identificate
import connection
import format_date

connection = connection.my_app()

app = connection[0]
mysql = connection[1]

def die(msg):
    print(msg)
    sys.exit(1)

class MakeResponse:
    """
    This class format the response
    Good and even Bad response
    """
    def __init__(self, my_code, my_datas=None):
        self.my_code = my_code
        self.my_datas = my_datas
    
    def get_response(self):
        """
        This method get the response
        """
        my_messages = {
            200: "success",
            201: "success",
            400: "error",
            401: "Unauthorized",
            403: "Forbidden",
            404: "not found",
            405: "Method Not Allowed"
        }

        my_response = {
            "code": self.my_code,
            "message": my_messages[self.my_code]
        }
        if self.my_datas != None or self.my_code == 400:
            if self.my_code == 400:
                self.my_datas = []
            my_response["datas"] = self.my_datas
        
        my_response = jsonify(my_response)
        my_response.status_code = self.my_code
        return my_response
 
# Etape 1 et 9
@app.route('/api/domains.<extension>', methods=['GET', 'POST'])
def domains(extension):
    """
    Return all domains from the database
    the return's format is in JSON
    """
    if request.method == 'GET':
        current_user = identificate.get_user_details()
        print(current_user)
        query = '''
            SELECT
                    *
            FROM domain;
        '''

        print("Query :" , query)
        if extension == "json":
            datas = []
            cursor = mysql.connection.cursor()
            cursor.execute(query)
            domains = cursor.fetchall()

            for domain in domains:
                datas.append({
                    "id": domain[0],
                    "slug": domain[4],
                    "name": domain[2],
                    "description": domain[3]
                })

            response = MakeResponse(200,datas)
            response = response.get_response()

            return response
        else:
            response = MakeResponse(400)
            response = response.get_response()

            return response
    
    if request.method == 'POST':
        datas = {}
        authorization = None

        if extension != "json":
            return MakeResponse(400).get_response()
        
        if request.headers.get('Content-Type') != "application/x-www-form-urlencoded":
            return MakeResponse(400).get_response()
        
        authorization = request.headers.get('Authorization')
        if authorization == None:
            return MakeResponse(401).get_response()

        verif_if_user_exist = """
            SELECT
                id
            FROM
                user
            WHERE
                password = "{0}"
        """.format(authorization)

        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(verif_if_user_exist)
        user_exist_or_not = cursor.fetchone()
        cursor.close()
        user_id = user_exist_or_not[0]

        if user_id == None:
            return MakeResponse(401).get_response()
        
        name_of_domain = request.form.get('name')
        slug = name_of_domain.replace(' ', '-')

        if len(name_of_domain) < 1:
            return MakeResponse(400).get_response()

        langs = request.form.getlist('lang[]')
        langs = []
        for key in request.form.keys():
            if re.match(r'[lang[](.)*[]]',key):
                for value in request.form.getlist(key):
                    langs.append(value)

        if len(langs) < 1:
            return MakeResponse(400).get_response()

        description = request.form.get('description')

        if len(description) < 1:
            return MakeResponse(400).get_response()
        
        counter_name = 1
        slug_in_while = slug
        while 1:
            verif_if_domain_exist = """
                SELECT
                    *
                FROM
                    domain
                WHERE
                    slug = "{0}"
            """.format(slug_in_while)

            conn = mysql.connection
            cursor = conn.cursor()
            cursor.execute(verif_if_domain_exist)
            domain_exist_or_not = cursor.fetchone()


            if domain_exist_or_not != None:
                slug_in_while = slug + "-" + str(counter_name)
                counter_name = counter_name + 1
                continue
            break

        slug = slug_in_while

        verif_if_lang_exist = """
            SELECT
                *
            FROM
                lang
        """
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(verif_if_lang_exist)
        langs_in_database = cursor.fetchall()
        cursor.close()

        langs_in_database_list = []
        counter = 0
        for lang in langs_in_database:
            langs_in_database_list.append(langs_in_database[counter][0])
            counter = counter + 1
            

        for lang in langs:
            if lang not in langs_in_database_list:
                return MakeResponse(400).get_response()

        create_domain = """
            INSERT INTO 
                domain
                    (user_id, name, description, slug, created_at)
            VALUES 
                    ({0},'{1}','{2}','{3}',{4})
        """.format(user_id, name_of_domain, description, slug, "NOW()")
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(create_domain)
        cursor.close()
        conn.commit()

        get_last_domain_created = """
            SELECT
                domain.id AS domain_id,
                user_id,
                name,
                description,
                slug,
                created_at,
                username,
                email
            FROM
                domain
            INNER JOIN
                user
            ON
                user.id = domain.user_id
            WHERE
                slug = "{0}"
        """.format(slug)

        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(get_last_domain_created)
        last_domain_created = cursor.fetchone()
        cursor.close()

        for lang in langs:
            add_domain_langs = """
                INSERT INTO
                        domain_lang
                            (domain_id, lang_id)
                VALUES
                        ({0}, '{1}')    
            """.format(last_domain_created[0], lang)
            conn = mysql.connection
            cursor = conn.cursor()
            cursor.execute(add_domain_langs)
            cursor.close()
            conn.commit()


        datas["langs"] = langs
        datas["id"] = last_domain_created[0]
        datas["slug"] = slug
        datas["name"] = name_of_domain
        datas["description"] = last_domain_created[3]
        datas["creator"] = {
            "id" : last_domain_created[1],
            "username" : last_domain_created[6],
            "email" : last_domain_created[7]
        }
        datas["created_at"] = format_date.format_date(last_domain_created[5])

        return MakeResponse(201,datas).get_response()
    

# Etape 2 et 8
@app.route('/api/domains/<domain_slug>.<extension>', methods=['GET'])
def mailer(domain_slug, extension):
    """
    Return all mail from the database
    the return's format is in JSON
    """
    if request.method == 'GET':

        current_user = identificate.get_user_details()
        if request.headers.get('Authorization'):
            authorization = request.headers.get('Authorization')
        else:
            authorization = None
        print(current_user)
        query = """
            SELECT 
                domain.id                           AS domain_id,
                domain.slug                         AS slug,
                domain.name                         AS name,
                domain.description                  AS decription,
                user.id                             AS user_id,
                user.username                       AS username,
                domain.created_at                   AS created_at,
                GROUP_CONCAT(domain_lang.lang_id)   AS domain_lang,
                user.email                          AS email
            FROM `user`
            LEFT JOIN domain 
                    ON user.id = domain.user_id
            LEFT JOIN domain_lang 
                    ON domain.id = domain_lang.domain_id
            """
        
        if authorization != None:
            verif_if_domain_exist = """
                SELECT
                    *
                FROM
                    domain
                WHERE
                    slug = "{0}"
            """.format(domain_slug)

            cursor = mysql.connection.cursor()
            cursor.execute(verif_if_domain_exist)
            domain_exist = cursor.fetchone()

            if not domain_exist:
                return MakeResponse(404).get_response()

            verif_if_the_domain_is_for_the_user = """
                SELECT
                (
                    SELECT 
                        id
                    FROM 
                        user
                    WHERE password = "{0}"
                ) 
                    = 
                (
                    SELECT 
                        id 
                    FROM
                        domain 
                    WHERE slug = "{1}"
                ) AS result
            """.format(authorization, domain_slug)

            cursor = mysql.connection.cursor()
            cursor.execute(verif_if_the_domain_is_for_the_user)
            domain_is_for_the_user_or_not = cursor.fetchone()

            if domain_is_for_the_user_or_not[0] != 1:
                authorization = None

        if authorization != None:
            next_query = """
                WHERE domain.slug LIKE '{0}'
                AND
                domain.user_id = (
                    SELECT
                            id
                    FROM
                            user
                    WHERE
                            password = "{1}"
                )
                GROUP BY domain.id;
            """.format(domain_slug, authorization)

        if authorization == None:
            next_query = """
                WHERE domain.slug LIKE '{0}'
                GROUP BY domain.id;
            """.format(domain_slug)
        
        query = query + next_query
        
        print(query)
        if extension == "json":
            datas = {}
            cursor = mysql.connection.cursor()
            cursor.execute(query)
            domain_slug = cursor.fetchall()

            if len(domain_slug) == 0:
                return MakeResponse(404).get_response()

            for slug in domain_slug:
                if slug[7] != None:
                    langs = slug[7].split(',')
                else:
                    langs = []

                datas["langs"] = langs
                datas["id"] = slug[0]
                datas["slug"] = slug[1]
                datas["name"] = slug[2]
                datas["description"] = slug[3]
                if authorization != None:
                    datas["creator"] = {
                        "id" : slug[4],
                        "username" : slug[5],
                        "email" : slug[8]
                    }                
                else:
                    datas["creator"] = {
                        "id" : slug[4],
                        "username" : slug[5]
                    }
                datas["created_at"] = format_date.format_date(slug[6])

            return MakeResponse(200, datas).get_response()

        else:
            return MakeResponse(400).get_response()

# Etape 3 et 7
@app.route('/api/domains/<slug>/translations.<extension>', methods=['GET'])
def translation(slug, extension):
    """
    return all translations from the slug given
    """
    if request.method == 'GET':
        arg_code = request.args.get('code')
        current_user = identificate.get_user_details()
        print(current_user)
        select_translation_query = """
        SELECT 
            translation.id                            AS translation_id,
            translation.code                          AS code,
            GROUP_CONCAT(translation_to_lang.lang_id) AS lang_id,
            GROUP_CONCAT(translation_to_lang.trans)   AS trans,
            domain.id                                 AS domain_id
        FROM domain
        LEFT JOIN translation 
            ON domain.id = translation.domain_id
        LEFT JOIN translation_to_lang 
            ON translation.id = translation_to_lang.translation_id"""
        if arg_code:
            next_query = """
                WHERE domain.slug = '{0}'
                AND
                    translation.code
                                    LIKE "%{1}%"
                GROUP BY translation.id, translation.code;
            """.format(slug, arg_code)
        else:
            next_query = """
                WHERE domain.slug = '{0}'
                GROUP BY translation.id, translation.code;
            """.format(slug)

        select_translation_query = select_translation_query + next_query
        print(select_translation_query)

        if extension == "json":
            datas = []
            trans = {}
            
            cursor = mysql.connection.cursor()
            cursor.execute(select_translation_query)
            domain_slug = cursor.fetchall()

            if len(domain_slug) < 1:
                response = MakeResponse(404)
                response = response.get_response()

                return response
            
            

            for row in domain_slug:
                trans = {}
                if row[2] != None:
                    langs = row[2].split(',')
                else:
                    langs = ""
                if row[3] != None:
                    langs_trans = row[3].split(',')
                else:
                    langs_trans = ""

                if len(langs) >= 1 and len(langs_trans) >= 1:
                    all_domain_lang = []
                    select_langs_query = """
                        SELECT
                                *
                        FROM
                                domain_lang
                        WHERE domain_id = {0}
                    """.format(row[4])

                    cursor.execute(select_langs_query)
                    domain_lang = cursor.fetchall()

                    for lang in domain_lang:
                        lang = lang[1]
                        all_domain_lang.append(lang)

                    for lang in all_domain_lang:
                        if lang not in langs:
                            trans[lang] = row[1]
                    for lang, lang_trans in zip(langs, langs_trans):
                        trans[lang] = lang_trans
                if row[0] != None and row[1] != None:
                    datas.append({
                        "trans" : trans,
                        "id" : row[0],
                        "code" : row[1]
                    })
                else:
                    datas = [] 

            response = MakeResponse(200, datas)
            response = response.get_response()

            return response
        else:
            response = MakeResponse(400)
            response = response.get_response()

            return response

# Etape 4
@app.route('/api/domains/<domain_slug>/translations.<extension>', methods=['POST'])
def addTranslation(domain_slug, extension):
    """
    Add a translation's code with some translations
    """

    if request.method == 'POST':

        datas = {}
        trans = {}
        print(request)

        if request.headers['Authorization']:
            authorization = request.headers['Authorization']
        else:
            response = MakeResponse(400)
            response = response.get_response()

            return response

        if request.headers["Content-type"]:
            content_type = request.headers["Content-type"]
        else:
            response = MakeResponse(400)
            response = response.get_response()

            return response

        if extension != "json" or content_type != "application/x-www-form-urlencoded":
            response = MakeResponse(400)
            response = response.get_response()

            return response

        current_user = identificate.get_user_details()
        print(current_user)
        get_user_query = """
            SELECT
                    *
            FROM    user
            WHERE   password = '{0}';
        """.format(authorization)

        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(get_user_query)
        user = cursor.fetchone()
        cursor.close()
        print("Get the user\n",get_user_query)
        
        if user is None:
            response = MakeResponse(401)
            response = response.get_response()

            return response
        
        user_id = user[0]

        verif_domain_exist = """
            SELECT 
                    *
            FROM    domain
            WHERE   slug = '{0}';
        """.format(domain_slug)

        cursor = conn.cursor()
        cursor.execute(verif_domain_exist)
        domain = cursor.fetchone()
        cursor.close()
        print("Verification if domain exist\n",verif_domain_exist)

        if domain is None:
            response = MakeResponse(404)
            response = response.get_response()

            return response
        
        domain_user_id = domain[1]
        domain_id = domain[0]

        if user_id != domain_user_id:
            response = MakeResponse(403)
            response = response.get_response()

            return response

        
        if "code" not in request.form or len(request.form["code"]) < 1:
            response = MakeResponse(400)
            response = response.get_response()

            return response
        
        request_code = request.form["code"]

        if not re.match(r"^(_)*[a-zA-Z0-9]+(_)*$", request_code):
            response = MakeResponse(400)
            response = response.get_response()

            return response
            

        verif_if_code_already_exist = """
            SELECT 
                    *
            FROM
                    translation
            WHERE code = '{0}'
            AND domain_id = {1}
        """.format(request_code, domain_id)
        cursor = conn.cursor()
        cursor.execute(verif_if_code_already_exist)
        result_if_code_already_exist = cursor.fetchone()
        cursor.close()
        print("Verification if code already exist\n", verif_if_code_already_exist)

        if result_if_code_already_exist:
            response = MakeResponse(400)
            response = response.get_response()
            return response

        try:
            cursor = conn.cursor()
            
            # First insert the translation exemple:
            # 1, __test__
            insert_data_query = """
                INSERT INTO 
                    translation (`domain_id`, `code`) 
                VALUES ({0},'{1}');
            """.format(domain_id, request_code)
            print("Insert data into translation table\n", insert_data_query)

            cursor.execute(insert_data_query)
            conn.commit()
            cursor.close()

            # Get the data id just added
            get_last_data_query_id = """
                SELECT LAST_INSERT_ID();
            """
            cursor = conn.cursor()
            cursor.execute(get_last_data_query_id)
            last_id = cursor.fetchone()[0]
            cursor.close()
            print("last id inserted", last_id)

            # Get all the lang and insert words for a language
            get_lang_available = """
                SELECT
                    code
                FROM
                    lang;
            """
            cursor = conn.cursor()
            cursor.execute(get_lang_available)
            langs = cursor.fetchall()
            cursor.close()

            for lang in langs:
                exec("{0} = '{0}'".format(lang[0]))
            
            for line in request.form:
                if re.match(r"^trans\[(.+)\]$",line):
                    line_value = request.form[line]
                    exec("{0} = '{1}'".format(line, line_value))
            
            for transl in trans:
                cursor = conn.cursor()
                insert_traduction_query = """
                    INSERT INTO
                        translation_to_lang
                            (translation_id , lang_id, trans)
                    VALUES
                        ({0}, '{1}', '{2}')
                """.format(last_id, transl , trans[transl])
                cursor.execute(insert_traduction_query)
                conn.commit()
                cursor.close()

        except Exception as e:
            print("L'erreur est la suivante\n", e)
            conn.rollback()
            cursor = conn.cursor()
            delete_last_data_id = """
                    DELETE FROM 
                        translation 
                    WHERE id = {0}
            """.format(last_id)
            print(delete_last_data_id)

            cursor.execute(delete_last_data_id)
            conn.commit()

            response = MakeResponse(400)
            response = response.get_response()

            return response

    cursor = conn.cursor()
    select_last_translation_added = """
        SELECT
            *
        FROM
            translation_to_lang
        WHERE translation_id = {0}
    """.format(last_id)

    cursor.execute(select_last_translation_added)
    result_last_translation_added = cursor.fetchall()

    print(result_last_translation_added)

    select_trads_linked = """
        SELECT
            translation.code            AS translation_code,
            translation.id 		        AS translation_id,
            translation_to_lang.lang_id AS lang,
            translation_to_lang.trans 	AS translations,
            translation.domain_id   
            FROM translation
            LEFT JOIN 
                translation_to_lang 
                ON 
                translation.id = translation_to_lang.translation_id
            WHERE translation.id = {0}
    """.format(last_id)

    cursor = conn.cursor()
    cursor.execute(select_trads_linked)
    result_langs_linked = cursor.fetchall()
    cursor.close()
    result_code = None
    result_domain_id = None


    for row in result_langs_linked:
        trans[row[2]] = row[3]
        result_code = row[0]
        result_domain_id = row[4]

    select_langs_linked = """
        SELECT
            lang_id
        FROM
            domain_lang
        WHERE
            domain_id = {0}
    """.format(result_domain_id)
    
    cursor = conn.cursor()
    cursor.execute(select_langs_linked)
    langs = cursor.fetchall()

    for lang in langs:
        lang = lang[0]
        if lang not in trans:
            trans[lang] = row[0]

    datas["trans"] = trans
    datas["id"] = last_id
    datas["code"] = result_code

    response = MakeResponse(201,datas)
    response = response.get_response()

    return response 

# Etape 5 et 6
@app.route('/api/domains/<domain_slug>/translations/<file_id>.<extension>', methods=['PUT', 'DELETE'])
def updateOrDeleteTranslation(domain_slug, file_id, extension):
    """
    Update,add or delete a translation to a code file
    """

    if request.method == 'PUT':
        if request.headers['Authorization']:
            authorization = request.headers['Authorization']
        else:
            print("erreur 1")
            response = MakeResponse(400)
            response = response.get_response()

            return response

        if request.headers["Content-type"]:
            content_type = request.headers["Content-type"]
        else:
            print("erreur 2")
            response = MakeResponse(400)
            response = response.get_response()

            return response

        if extension != "json" or content_type != "application/x-www-form-urlencoded":
            print("erreur 3")
            response = MakeResponse(400)
            response = response.get_response()

            return response

        conn = mysql.connection


        verif_if_user_exist = """
            SELECT
                    *
            FROM
                    user
            WHERE
                    password = '{0}';
        """.format(authorization)

        cursor = conn.cursor()
        cursor.execute(verif_if_user_exist)
        user = cursor.fetchone()
        cursor.close()
        user_id = user[0]

        print("Verif if the user exists\n", verif_if_user_exist)


        if not user:
            print("erreur 4")
            response = MakeResponse(401)
            response = response.get_response()

            return response

        print("User :\n",user)

        verif_if_domain_exist = """
            SELECT
                    *
            FROM
                    domain
            WHERE
                    slug = '{0}';
        """.format(domain_slug)

        print("Verif if domain exists\n", verif_if_domain_exist)

        cursor = conn.cursor()
        cursor.execute(verif_if_domain_exist)
        domain = cursor.fetchone()
        cursor.close()

        if not domain:
            print("erreur 5")
            response = MakeResponse(400)
            response = response.get_response()

            return response

        domain_id = domain[0]
        domaun_user_id = domain[1]
        print(domain)

        verif_if_code_exists = """
            SELECT
                    *
            FROM
                    translation
            WHERE
                    id = {0};
        """.format(file_id)

        cursor = conn.cursor()
        cursor.execute(verif_if_code_exists)
        code = cursor.fetchone()
        cursor.close()

        # Get all the lang and insert words for a language
        get_lang_available = """
            SELECT
                lang_id
            FROM
                domain_lang
            WHERE
                domain_id = {0};
        """.format(domain_id)
        cursor = conn.cursor()
        cursor.execute(get_lang_available)
        langs = cursor.fetchall()
        cursor.close()

        trans = {}

        for lang in langs:
            lang = lang[0]
            exec("{0} = '{0}'".format(lang))
        try:
            for line in request.form:
                if re.match(r"^trans\[(.+)\]$",line):
                    line_value = request.form[line]
                    if len(line_value) < 1:
                        raise Exception("error")
                    exec("{0} = '{1}'".format(line, line_value))
        except:
            print("erreur 6")
            response = MakeResponse(400)
            response = response.get_response()

            return response

        print("trans\n",trans)

        trans_key = list(trans.keys())
        
        if domaun_user_id != user_id:
            response = MakeResponse(403)
            response = response.get_response()

            return response

        for l in trans_key:
            try:
                update_query = """ 
                    REPLACE INTO 
                            translation_to_lang 
                            (translation_id, lang_id, trans)
                    SELECT 
                            {0}, '{1}', '{2}'
                """.format(file_id, l, trans[l] )
                
                print(update_query)
                cursor = conn.cursor()
                cursor.execute(update_query)
                conn.commit()
                cursor.close()

            except:
                print("erreur 7")
                conn.rollback()
                response = MakeResponse(400)
                response = response.get_response()

                return response

        select_trads_linked = """
            SELECT
                translation.code            AS translation_code,
                translation.id 		        AS translation_id,
                translation_to_lang.lang_id AS lang,
                translation_to_lang.trans 	AS translations,
                translation.domain_id   
                FROM translation
                LEFT JOIN 
                    translation_to_lang 
                    ON 
                    translation.id = translation_to_lang.translation_id
                WHERE translation.id = {0}
        """.format(file_id)

        cursor = conn.cursor()
        cursor.execute(select_trads_linked)
        result_langs_linked = cursor.fetchall()
        cursor.close()

        datas = {}
        result_code = None
        result_domain_id = None


        for row in result_langs_linked:
            trans[row[2]] = row[3]
            result_code = row[0]
            result_domain_id = row[4]

        select_langs_linked = """
            SELECT
                lang_id
            FROM
                domain_lang
            WHERE
                domain_id = {0}
        """.format(domain_id)
        
        cursor = conn.cursor()
        cursor.execute(select_langs_linked)
        langs = cursor.fetchall()

        for lang in langs:
            lang = lang[0]
            if lang not in trans:
                trans[lang] = row[0]

        datas["trans"] = trans
        datas["id"] = int(file_id)
        datas["code"] = result_code


        response = MakeResponse(200, datas)
        response = response.get_response()

        return response

    if request.method == 'DELETE':

        if request.headers['Authorization']:
            authorization = request.headers['Authorization']
        else:
            response = MakeResponse(400)
            response = response.get_response()

            return response

        if extension != "json":
            response = MakeResponse(400)
            response = response.get_response()

            return response

        datas = {}
        conn = mysql.connection

        verif_if_file_exists = """
            SELECT
                    *
            FROM
                    translation
            WHERE
                    id = {0}
        """.format(file_id)

        cursor = conn.cursor()
        cursor.execute(verif_if_file_exists)
        result_if_file_exists = cursor.fetchone()
        cursor.close()

        if not result_if_file_exists:
            return MakeResponse(404).get_response()

        verif_if_user_ok = """
            SELECT
                *
            FROM
                domain
            WHERE user_id = (
                            SELECT
                                id
                            FROM
                                user
                            WHERE
                                password = "{0}")
            AND
                slug = "{1}";
        """.format(authorization, domain_slug)

        print("Verif if user is ok\n", verif_if_user_ok)

        cursor = conn.cursor()
        cursor.execute(verif_if_user_ok)
        user_ok_or_not = cursor.fetchone()
        cursor.close()

        print("User is ok or not ?\n",user_ok_or_not)
        
        if not user_ok_or_not:
            return MakeResponse(403).get_response()


        query_delete_translation_to_lang = """
            START TRANSACTION;
                DELETE FROM translation_to_lang WHERE translation_id = {0};
                DELETE FROM translation WHERE id = {0};
            COMMIT;
        """.format(file_id)

        cursor = conn.cursor()
        cursor.execute(query_delete_translation_to_lang)
        cursor.close()

        datas["id"] = file_id

        response = MakeResponse(200, datas)
        response = response.get_response()

        return response
    else:
        response = MakeResponse(404)
        response = response.get_response()

        return response

# Etape 10
@app.route('/api/domains/<domain_slug>/langs/<lang>.<extension>', methods=["DELETE"])
def deleteLang(domain_slug, lang, extension):
    """
    Delete a lang from a domain
    """
    if request.method == 'DELETE':
        if extension != "json":
            return MakeResponse(404).get_response()
        if request.headers.get('Content-type') != "application/form-data":
            return MakeResponse(400).get_response()
        if request.headers.get('Authorization'):
            authorization = request.headers.get('Authorization')
        else:    
            return MakeResponse(401).get_response()

        datas = {}
        
        verif_if_user_exist = """
            SELECT
                id
            FROM
                user
            WHERE
                password = "{0}"
        """.format(authorization)
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(verif_if_user_exist)
        user_id = cursor.fetchone()
        cursor.close()
        user_id = user_id[0]

        if user_id == None:
            return MakeResponse(401).get_response()
        
        verif_if_domain_exist = """
            SELECT
                id, user_id
            FROM
                domain
            WHERE
                slug = "{0}"
        """.format(domain_slug)
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(verif_if_domain_exist)
        domain = cursor.fetchone()
        cursor.close()

        if domain == None:
            return MakeResponse(404).get_response()

        domain_id = domain[0]
        domain_user_id = domain[1]

        if domain_user_id != user_id:
            return MakeResponse(403).get_response()
        
        verif_if_lang_exist = """
            SELECT
                *
            FROM
                domain_lang
            WHERE
                domain_id = {0}
            AND
                lang_id = '{1}'
        """.format(domain_id, lang)
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(verif_if_lang_exist)
        lang_exist = cursor.fetchone()
        cursor.close()

        if lang_exist == None:
            return MakeResponse(404).get_response()
        
        delete_lang = """
            DELETE FROM
                domain_lang
            WHERE
                domain_id = {0}
            AND
                lang_id = '{1}'
        """.format(domain_id, lang)
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(delete_lang)
        conn.commit()
        cursor.close()

        all_langs_domain = """
            SELECT
                domain_id,
                GROUP_CONCAT(lang_id)
            FROM
                domain_lang
            WHERE
                domain_id = {0}
            GROUP BY domain_id
        """.format(domain_id)
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(all_langs_domain)
        all_langs = cursor.fetchone()
        cursor.close()

        if all_langs != None:
            langs = all_langs[1].split(',')
        else:
            langs = []

        get_all_informations_to_return = """
            SELECT
                *
            FROM
                domain
            INNER JOIN
                user
            ON
                domain.user_id = user.id
            WHERE
                domain.id = {0}
            AND
                domain.slug = '{1}'
        """.format(domain_id, domain_slug)
        print(get_all_informations_to_return)
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(get_all_informations_to_return)
        all_informations = cursor.fetchone()

        datas['langs'] = langs
        datas['id'] = domain_id
        datas['slug'] = all_informations[4]
        datas['name'] = all_informations[2]
        datas['description'] = all_informations[3]
        datas['creator'] = {
            "id" : all_informations[1] ,
            "username" : all_informations[7],
            "email": all_informations[8]
        }
        datas["created_at"] = format_date.format_date(all_informations[5])

        return MakeResponse(200,datas).get_response()


@app.errorhandler(404)
def page_not_found(e):
    """
    Redefine the 404 error
    """
    response = MakeResponse(404)
    response = response.get_response()

    return response

@app.errorhandler(401)
def unauthorized(e):
    """
    Redefine the 401 error
    """
    response = MakeResponse(401)
    response = response.get_response()

    return response

@app.errorhandler(400)
def bad_request(e):
    """
    Redefine the 400 error
    """
    response = MakeResponse(400)
    response = response.get_response()

    return response

@app.errorhandler(405)
def method_not_allowed(e):
    """
    Redefine the 405 error
    """
    response = MakeResponse(405)
    response = response.get_response()

    return response

@app.errorhandler(Exception)
def all_errors(e):
    """
    Redefine all errors
    """

    e = e.args[0]
    print(e)
    response = MakeResponse(401)
    response = response.get_response()

    return response

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0" , port=80)
    app.config['JSON_SORT_KEYS'] = False
