import socket
import datetime

def get_user_details():
    """
    Run when it's called in our case it's when someone
    try to connect at our API
    """
    user_details = {}
    hostname = socket.gethostname()   
    IPAddr = socket.gethostbyname(hostname)

    user_details["name"] = "The Computer Name is: {0}".format(hostname)
    user_details["ip"] = "The Computer IP Address is: {0}".format(IPAddr)
    user_details["data"] = "Tryed to connect at: {0}".format(datetime.datetime.now())

    return user_details