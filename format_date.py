from datetime import tzinfo, timedelta, datetime


class TZ(tzinfo):
    """
    Class whom return the timezone or
    return the timedelta
    """
    def utcoffset(self, dt):
	    """
	    Return the timedelta
	    """
	    return timedelta(minutes=60)

def format_date(date):
    """
    Return the date format ISO 8610
    """
    myDate = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, tzinfo=TZ()).isoformat()
    return myDate